/*This code */

/* make sure to have FastLed and Adafruit FreeTouch installed*/
#include <FastLED.h>
#include "Adafruit_FreeTouch.h"

#define NUM_LEDS 1
#define DATA_PIN A1
#define VBATT_PIN A7
#define BRIGHTNESS 200
#define FRAMES_PER_SECOND  120

Adafruit_FreeTouch qt_1 = Adafruit_FreeTouch(A5, OVERSAMPLE_4, RESISTOR_0, FREQ_MODE_NONE);
Adafruit_FreeTouch qt_2 = Adafruit_FreeTouch(A3, OVERSAMPLE_4, RESISTOR_0, FREQ_MODE_NONE);

CRGB leds[NUM_LEDS];
uint8_t BRIGHT = 200;
uint8_t gHue = 0;

void setup() {
  // Setup
  delay(3000);
  FastLED.addLeds<WS2812, DATA_PIN, RGB>(leds, NUM_LEDS);  // GRB ordering is typical
  FastLED.setBrightness(BRIGHTNESS);

  //Measure Voltage on VCAP, should be between 0.8V and 1.4V
  int vCap = analogRead(VBATT_PIN);
  float voltage = vCap * (3.3 / 1023.0);

  if (voltage >= 1.40) { //Blue LED on full charge
    leds[0] = CRGB(0,0,100);
    FastLED.show();
    Serial.println(voltage);
    FastLED.delay(100);
  }
  else if (voltage < 1.40 && voltage > 1.35) { // Green Led if relaxed charge
    leds[0] = CRGB(0,100,0);
    FastLED.show();
    Serial.println(voltage);
    FastLED.delay(100);
  }
  else if (voltage < 1.35 && voltage > 1.0) { // Yellow if could use some charge time
    leds[0] = CRGB(50,55,0);
    FastLED.show();
    Serial.println(voltage);
    FastLED.delay(100);
  }  
  else if (voltage <= 0.9) { // Red if drained
    leds[0] = CRGB(50,0,0);
    FastLED.show();
    Serial.println(voltage);
    FastLED.delay(100);
  }
  
  FastLED.delay(2000);
  
  qt_1.begin();
  qt_2.begin();
  
  leds[0] = CRGB(0,0,0);
  FastLED.show(); // Turn off LED
}

void loop() { //main loop, runs rainbow cycle, adjusts brightness if one of touch pads are held

  // do some periodic updates
  rainbow();
  FastLED.show();
  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
  
  if (qt_1.measure() > 500) { //If touch increment brightness own
    if(BRIGHT > 0) {
    int newBright = BRIGHT--;
    FastLED.setBrightness(newBright);
    FastLED.delay(1000/FRAMES_PER_SECOND); 
    }
  }
  else if (qt_2.measure() > 500) { // if touch increment brightness up
    if (BRIGHT < 255) {
    int newBright = BRIGHT++; 
    FastLED.setBrightness(newBright);
    FastLED.delay(1000/FRAMES_PER_SECOND); 
    }
  }

}
void rainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}
