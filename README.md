Base Folder for example Code for Noor Martin Arduino Projects

The folders here contain basic arduino sketches for their respective hardware. I'll work on fleshing out comments and descriptions but for now just uploaded to get them out there.

Programming Instructions:

Khamsa_1v4:

The khamsa1v4 is a samd21 arduino board and the pinout is based on the adafruit feather m0. You can follow these instructions for installation: [adafruit m0 board package install guide]("https://learn.adafruit.com/adafruit-feather-m0-basic-proto/using-with-arduino-ide")

Tiny90:

The tiny90 uses an Atmel ATTiny 3217, and for programming uses the [MegaTiny AVR board package]("https://github.com/SpenceKonde/megaTinyCore"). For quick installation if you know what you are doing here is the board package url: http://drazzy.com/package_drazzy.com_index.json

The tiny90 has a Silicon Labs CP2105, which provides 2 com ports over 1 USB port. One comport is the serial port and the other is connected to the UDPI pin to allow for programming over usb(read more about programming on the megaTinyCore github). Currently in my tests Arduino identifies the serial port as the Tian MIPS console and the UDPI port as the Tian console(i may have these mixed up, just pulling from memory).
